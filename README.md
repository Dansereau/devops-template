# Devops Template

## Instructions

See subfolders kurbenetes, react-app, nodejs-app and rabbitmq so specific instructions.

### Tested on local minikube cluster

To generate a new cluster  
`minikube start --memory 6144 --cpus 4 [-p <minikube-cluster/vm-name>]`

To start the cluster  
`minikube start [-p <minikube-cluster/vm-name>]`

To open the dashboard  
`minikube dashboard [-p <minikube-cluster/vm-name>]`  

To get the cluster ip  
`minkube ip [-p <profile-name>]`

To get exposed external ports  
`kubectl get svc --all-namespaces -o go-template='{{range .items}}{{range.spec.ports}}{{if .nodePort}}{{.nodePort}}{{"\n"}}{{end}}{{end}}{{end}}'`

## Notes

Automatically builds and saves containers on registry.gitlab.com/  

## TODOS

1. Gitlab-ci Ansible deloy
2. Gitlab-ci automated test
3. Map service names in deployment and update apps accordingly + tests
4. Ansible/python master-node container to deploy to cluster
5. Configure websocket frontend/backend
6. PostGreSQL container + service
7. Redis container + service