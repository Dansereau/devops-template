import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Publisher from './restApi.js';
class App extends Component {

  handleClick = () => {
    let messenger = new Publisher();
    messenger.publish();
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.<br />
            This is my first container reload!!!!!!! Yay
          </p>
          <button onClick={this.handleClick.bind(this)}>Hello</button>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  }
}

export default App;
