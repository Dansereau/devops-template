export default class Publisher {

  publish() {
    var url = 'http://nodejsapp-service';
    var data = { username: 'example' };

    fetch(url, {
      method: 'GET', // or 'PUT'
      // body: JSON.stringify(data), // data can be `string` or {object}!
      // headers: {
      //   'Content-Type': 'application/json',
      // }
    }).then(res => res.json())
      .then(response => console.log(response))
    // }).then(res => console.log(res))
      .catch(error => console.error('Error:', error));
  }
}

