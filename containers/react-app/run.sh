#!/usr/bin/env bash
set -eo pipefail

case $1 in 
  start) 
    # The '| cat'  is to trick Node that this is an non-TTY terminal
    # then react-scripts won't clear the console.
    serve -s build -l 3000
    ;;
  dev)
    yarn start | cat
    ;;
  test)
    yarn test $@
    ;;
  *)
    exec "$@"
    ;;
esac


