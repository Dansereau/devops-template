# Docker React App

## Instructions

To build the Docker image
`docker image build -t <app-name> .`

To run the container in development mode
`docker run -it --rm -p 3000:3000 <image-name> dev`

To run the container with warm reload
`docker run -it --rm -p 3000:3000 -p 35729:35729 -v (pwd):/app <image-name> dev` *(fish shell)*
`docker run -it --rm -p 3000:3000 -p 35729:35729 -v $(pwd):/app <image-name> dev` *(bash shell)*

To make non-persistent changes on a container running with hot reload  
`docker exec -it <container-name/id> bash`

### To build the container in production mode with static resources

First build the static resources
`sudo yarn build; docker image build -t <image-name> -f Dockerfile.prod .`

Fourth, enjoy and run the production build to test!
`docker run -it --rm -p 5000:5000 watman00paradise/csparadise:reactapp-prod`

### Tests
To run interactive jest tests [--coverage|--help] *(--watch is on by default)*
`docker run -it --rm -v (pwd):/app <image-name> test` *(fish shell)*
`docker run -it --rm -v $(pwd):/app <image-name> test`  *(bash shell)*

To run NodeJS Shell
`docker run -it --rm -v (pwd):/app <image-name> bash` *(fish shell)*
`docker run -it --rm -v $(pwd):/app <image-name> bash` *(bash shell)*

Then `node`