/* eslint-disable no-console */
var amqp = require ('amqplib/callback_api');

const Publisher = {
  
  publish: function () {

    // eslint-disable-next-line no-global-assign
    URL = 'amqp://admin:admin@172.17.0.2:5672/%2Fvhost1';
    amqp.connect(URL, function (error, connection) {
      connection.createChannel(function (error, channel) {
        
        let queue = 'gitlab';
        
        // channel.assertQueue(queue, {durable: false});
        channel.sendToQueue(queue, Buffer.from('Hello World!'))
        console.log(" [x] Sent 'Hello World!'");
      });
      
      setTimeout(function () { connection.close(); process.exit(0) }, 500);
    });
  }
}

module.export = Publisher;