/* eslint-disable no-console */
/*eslint no-undef: "error"*/
/*eslint-env node*/

var amqp = require('amqplib/callback_api');
function send () {

  amqp.connect('amqp://admin:admin@172.17.0.2:5672/%2fvhost1', function(err, conn) {
    conn.createChannel(function(error, channel) {
      var queue = 'gitlab';
      var msg = "SNAFU";
      channel.assertQueue(queue, {durable: true});
      // Note: on Node 6 Buffer.from(msg) should be used
      channel.sendToQueue(queue, new Buffer(msg));
      console.log(" [x] Sent '%s'", msg);
    });
    // setTimeout(function() { conn.close(); process.exit(0) }, 500);
  });
}
module.exports = send;