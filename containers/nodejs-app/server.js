/* eslint-disable no-console */
/*eslint no-undef: "error"*/
/*eslint-env node*/
'use strict';
const express = require('express');
var cors = require('cors');
var send = require('./send.js');
var amqp = require('amqplib/callback_api');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();

var RabbitMQListenner = () => {
  amqp.connect('amqp://admin:admin@172.17.0.2:5672/%2fvhost1', function (connectionError, connection) {
    connection.createChannel(function (createChannelError, channel) {
      var queue = 'gitlab';

      channel.assertQueue(queue, { durable: true });
      console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);
      channel.consume(queue, function (msg) {
        console.log(" [x] Received '%s'", msg.content.toString());
      }, { noAck: true });
    });
  });
}

// use it before all route definitions
app.use(cors());
app.get('/', (req, res) => {

  // eslint-disable-next-line no-global-assign
  send();
  RabbitMQListenner();

  // console.log('IFW :', IFW);
  res.json({ "FU": "BAR" });
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);