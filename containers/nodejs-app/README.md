# Docker Nodejs Express App

## Instructions

To build the image
`docker build -t <image-name> .`

To run the container in development mode
`docker run -p 49160:8080 -it --rm <image-name>`

To run the container in development mode with hot reload
`docker run -it --rm -p 49160:8080 -v (pwd):/usr/src/app <image-name>` *(fish shell)*
`docker run -it --rm -p 49160:8080 -v $(pwd):/usr/src/app <image-name>` *(bash shell)*

To run the container in production mode
`docker run -p 49160:8080 -d <image-name> build`

To test the image
`curl -i localhost:49160`
