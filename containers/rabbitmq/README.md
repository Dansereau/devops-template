# Docker RabbitMQ

## Instructions

To build the image
`docker build -t <image-name> .`

To run the container in development mode
`docker run -it --rm -p 8080:15672 <image-name>`

`15672` is the port to access the management interface

`5672` is the connection port (non-tls)

Default login is `admin:admin`

## Uploading the image to Dockerhub

1. First login with your account

`docker login`

2. Tag the image

`docker tag <image-name> <your-username>/<repository-name>:<version>`

3. Push the image to Dockerhub

`docker push <your-username>/<repository-name>:<version>`