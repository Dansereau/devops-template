# devops-template

## Instructions

### Runnning devops-training playbook

Prerequisite: Python openshift `sudo pip install openshift`

Ansible command *(Run from your machine)*  
`ansible-playbook devops-training.yml -i ../inventory/local-minikube`

### Accessing the public services

Get your minikube node ip  
`minkube ip [-p <profile-name>]`

Open a browser and enter the following address  
React App: `<minikube-ip>:31590`  
RabbitMQ: `<minikube-ip>:30672`

### Manual Deployment

To create a namespace with existing config  
`kubectl create -f namespace.yml -o yaml`

To create a resource quota with existing config  
`kubectl create -f resourceQuota.yml -o yaml`

To check if the resource quota has applied to the namespace  
`kubectl describe ns <namespace-name>`

To create a deployment  
`kubectl create -f deployment.yml -o yaml`

To delete a deployment  
`kubectl delete deployment -n <namespace> <deployment-name>`

To verify the deployment  
`kubectl get deployments -n <namespace-name>`

To get a description of an existing deployment  
`kubectl get deployment <deployment-name> -n <namespace>  --output yaml`

To create a service  
`kubectl create -f service.yml`

To  describe service  
`kubectl describe service <service-name>`

To get exposed external ports  
`kubectl get svc --all-namespaces -o go-template='{{range .items}}{{range.spec.ports}}{{if .nodePort}}{{.nodePort}}{{"\n"}}{{end}}{{end}}{{end}}'`
